﻿FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
WORKDIR /app

# Copy everything
COPY WgMuellNotifier.sln ./WgMuellNotifier.sln
COPY Muelli.Api/Muelli.Api.csproj Muelli.Api/Muelli.Api.csproj
COPY Muelli.Application/Muelli.Application.csproj Muelli.Application/Muelli.Application.csproj
COPY Muelli.Domain/Muelli.Domain.csproj Muelli.Domain/Muelli.Domain.csproj
COPY Muelli.Infrastructure/Muelli.Infrastructure.csproj Muelli.Infrastructure/Muelli.Infrastructure.csproj
COPY Muelli.Test/Muelli.Test.csproj Muelli.Test/Muelli.Test.csproj

RUN dotnet restore Muelli.Domain/Muelli.Domain.csproj
RUN dotnet restore Muelli.Application/Muelli.Application.csproj  
RUN dotnet restore Muelli.Infrastructure/Muelli.Infrastructure.csproj  
RUN dotnet restore Muelli.Api/Muelli.Api.csproj
RUN dotnet restore Muelli.Test/Muelli.Test.csproj    

COPY . ./
# Restore as distinct layers
# Build and publish a release
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "Muelli.Api.dll"]
