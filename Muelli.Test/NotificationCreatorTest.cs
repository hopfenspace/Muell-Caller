using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Muelli.Application;
using Muelli.Application.Configs;
using Muelli.Application.Services;
using Muelli.Domain;
using Muelli.Infrastructure.Persistence;

namespace Muelli.Test;

public class NotificationCreatorTest
{
    [Fact]
    public async Task Test1()
    {
        var notificationSink = new Mock<INotificationSink>();
        bool _isDone = false;
        notificationSink.Setup(x => x.DoNotification(It.IsAny<Appointment>())).Callback(() => _isDone = true);

        var optionsBuilder = new DbContextOptionsBuilder<MuellDbContext>();
        optionsBuilder.UseInMemoryDatabase("Data Source=test.db");
        MuellDbContext muellDbContext = new MuellDbContext(optionsBuilder.Options);
        
        muellDbContext.Appointments.Add(new Appointment("1234", DateTime.Now.AddSeconds(-11), "asdas", "123"));
        await muellDbContext.SaveChangesAsync();

        NotificationCreator notificationCreator = new NotificationCreator(muellDbContext,
            notificationSink.Object, new NotificationConfig()
            {
                Notifications = new[] {"00:00:10"},
            }, NullLogger<NotificationCreator>.Instance);
        await notificationCreator.CheckForNotification();
        Assert.True(_isDone);
    }
}