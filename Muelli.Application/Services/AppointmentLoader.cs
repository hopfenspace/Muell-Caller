using Ical.Net;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Muelli.Application.Configs;
using Muelli.Domain;

namespace Muelli.Application.Services;

public class AppointmentLoader
{
    private readonly IMuellDbContext _muellDbContext;
    private readonly TelegramConfigs _telegramConfigs;
    private readonly ILogger<AppointmentLoader> _logger;

    public AppointmentLoader(IMuellDbContext muellDbContext, TelegramConfigs telegramConfigs,
        ILogger<AppointmentLoader> logger)
    {
        _muellDbContext = muellDbContext;
        _telegramConfigs = telegramConfigs;
        _logger = logger;
    }

    public async Task LoadAppointments()
    {
        _logger.LogInformation("Start Requesting Appointments");
        HttpClient httpClient = new HttpClient();
        foreach (var telegramConfig in _telegramConfigs.Configs)
        {
            _logger.LogInformation($"Start Requesting for Group {telegramConfig.ConfigId}");
            var stringCalender = await httpClient.GetStringAsync(telegramConfig.Url);
            var calendar = Calendar.Load(stringCalender);
            foreach (var calendarEvent in calendar.Events)
            {
                var alreadyAdded = await _muellDbContext.Appointments.AnyAsync(x =>
                    x.Text == calendarEvent.Summary && x.StartTime.Date == calendarEvent.Start.Date && x.ConfigId == telegramConfig.ConfigId);
                if (!alreadyAdded)
                {
                    _logger.LogInformation(
                        $"Add Appointment with Id {calendarEvent.Uid} in Config {telegramConfig.ConfigId}");
                    await _muellDbContext.Appointments.AddAsync(new Appointment(calendarEvent.Uid,
                        calendarEvent.Start.Date, calendarEvent.Summary, telegramConfig.ConfigId));

                    await _muellDbContext.SaveChangesAsync();
                }
            }
        }
    }
}