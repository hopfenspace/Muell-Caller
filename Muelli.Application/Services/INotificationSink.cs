using Muelli.Domain;

namespace Muelli.Application.Services;

public interface INotificationSink
{
    Task DoNotification(Appointment appointment);
}