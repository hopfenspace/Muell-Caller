using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Muelli.Application.Configs;
using Muelli.Domain;

namespace Muelli.Application.Services;

public class NotificationCreator
{
    private readonly IMuellDbContext _muellDbContext;
    private readonly INotificationSink _notificationSink;
    private readonly NotificationConfig _notificationConfig;
    private readonly ILogger<NotificationCreator> _logger;

    public NotificationCreator(IMuellDbContext muellDbContext, INotificationSink notificationSink,
        NotificationConfig notificationConfig, ILogger<NotificationCreator> logger)
    {
        _muellDbContext = muellDbContext;
        _notificationSink = notificationSink;
        _notificationConfig = notificationConfig;
        _logger = logger;
    }

    public async Task CheckForNotification()
    {
        _logger.LogInformation("Check for Notifications");
        var appointments = await _muellDbContext.Appointments
            .Where(x => x.Done == null && x.StartTime.Date > DateTime.Now.AddDays(-4))
            .Include(x => x.Notifications).ToListAsync();
        _logger.LogInformation($"{appointments.Count} Appointments to check");
        foreach (var appointment in appointments)
        {
            foreach (var timeSpan in _notificationConfig.NotificationTime)
            {
                if (appointment.Notifications.All(x => x.TimeSpan != timeSpan))
                {
                    if (appointment.StartTime.ToUniversalTime().Subtract(timeSpan) < DateTime.UtcNow)
                    {
                        var notification = appointment.CreateNotification(timeSpan);
                        if (notification != null)
                            await _muellDbContext.Notifications.AddAsync(notification);
                        await _muellDbContext.SaveChangesAsync();
                        _logger.LogInformation($"Send Appointment {appointment.AppointmentId}");
                        await _notificationSink.DoNotification(appointment);
                    }
                }
            }
        }
    }
}