namespace Muelli.Application.Configs;

public class TelegramConfigs
{
    public TelegramConfig[] Configs { get; set; }
}

public class TelegramConfig
{
    public string GroupId { get; set; }
    public string Url { get; set; }
    public string ConfigId { get; set; }
}