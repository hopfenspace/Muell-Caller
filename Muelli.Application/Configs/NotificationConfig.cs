namespace Muelli.Application.Configs;

public class NotificationConfig
{
    public string[] Notifications { get; set; }

    public List<TimeSpan> NotificationTime => Notifications.Select(x => TimeSpan.Parse(x)).ToList();
}