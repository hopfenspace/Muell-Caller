using Microsoft.EntityFrameworkCore;
using Muelli.Domain;

namespace Muelli.Application;

public interface IMuellDbContext
{
    public DbSet<Appointment> Appointments { get; set; }
    public DbSet<Notification> Notifications { get; set; }
    Task<int> SaveChangesAsync();
}