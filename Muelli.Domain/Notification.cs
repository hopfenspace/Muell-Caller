namespace Muelli.Domain;

public class Notification
{
    public Notification(TimeSpan timeSpan)
    {
        TimeSpan = timeSpan;
        NotificationId = Guid.NewGuid();
    }

    public Guid NotificationId { get; private set; }
    public TimeSpan TimeSpan { get; private set; }
}