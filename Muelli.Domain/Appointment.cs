using System.Collections.ObjectModel;

namespace Muelli.Domain;

public class Appointment
{
    public Guid AppointmentId { get; private set; }
    public string CalenderId { get; private set; }
    public DateTime StartTime { get; private set; }
    public DateTime? Done { get; private set; }
    public string? DoneBy { get; private set; }
    public string Text { get; private set; }
    public List<Notification> Notifications { get; private set; } = new List<Notification>();
 
    public string ConfigId { get; private set; }

    public Appointment(string calenderId, DateTime startTime, string text, string configId)
    {
        AppointmentId = Guid.NewGuid();
        CalenderId = calenderId;
        StartTime = startTime;
        Text = text;
        ConfigId = configId;
    }

    public void DoAppointment(string doneby)
    {
        DoneBy = doneby;
        Done = DateTime.Now;
    }

    public Notification CreateNotification(TimeSpan timeSpan)
    {
        if (!Notifications.Any(x => x.TimeSpan == timeSpan))
        {
            var notification = new Notification(timeSpan);
            Notifications.Add(notification);
            return notification;
        }

        return null;
    }
}