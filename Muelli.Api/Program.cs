using Microsoft.EntityFrameworkCore;
using Muelli.Api.HostedServices;
using Muelli.Infrastructure;
using Muelli.Infrastructure.Persistence;

var builder = WebApplication.CreateBuilder(args);

// Fix Enviroment Loading
Dictionary<string, string> normalized = builder.Configuration.AsEnumerable()
    .Where(p => p.Key.Contains("__"))
    .ToDictionary(p => p.Key.Replace("__", ":"), p => p.Value);
builder.Configuration.AddInMemoryCollection(normalized);

// Load Config
builder.Services.ConfigureServices(builder.Configuration);

builder.Services.AddHostedService<TelegramHostedService>();
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    scope.ServiceProvider.GetRequiredService<MuellDbContext>().Database.Migrate();
}



// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();

app.MapControllers();

app.Run();