using Muelli.Application.Configs;
using Muelli.Application.Services;
using Muelli.Infrastructure;
using Muelli.Infrastructure.Config;

namespace Muelli.Api.HostedServices;

public class TelegramHostedService : BackgroundService
{
    private readonly IServiceProvider _serviceProvider;
    private readonly TelegramRunner _telegramRunner;

    public TelegramHostedService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
        var botConfig = serviceProvider.GetService<BotConfig>();
        var telegramConfig = serviceProvider.GetService<TelegramConfigs>();
        _telegramRunner = new TelegramRunner(botConfig,telegramConfig,serviceProvider);
    }

    
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _telegramRunner.Start();
        while (!stoppingToken.IsCancellationRequested)
        {
            // do async work
            using (var scope = _serviceProvider.CreateScope())
            {
                var loader = scope.ServiceProvider.GetRequiredService<AppointmentLoader>();
                await loader.LoadAppointments();
                await Task.Delay(TimeSpan.FromMinutes(5), stoppingToken);
                var notifier = scope.ServiceProvider.GetRequiredService<NotificationCreator>();
                await notifier.CheckForNotification();
            }
        }
    }
}