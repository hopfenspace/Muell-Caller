using Microsoft.AspNetCore.Mvc;
using Muelli.Application.Services;

namespace Muelli.Api;
[ApiController]
[Route("Test")]
public class TestController:ControllerBase
{
    private readonly AppointmentLoader _appointmentLoader;
    private readonly NotificationCreator _notificationCreator;

    public TestController(AppointmentLoader appointmentLoader, NotificationCreator notificationCreator)
    {
        _appointmentLoader = appointmentLoader;
        _notificationCreator = notificationCreator;
    }

    [HttpGet("Run")]
    public async Task Run()
    {
        await _appointmentLoader.LoadAppointments();
        await _notificationCreator.CheckForNotification();
    }
    [HttpGet("Test")]
    public OkObjectResult Test()
    {
        return Ok("Hallo");
    }
}