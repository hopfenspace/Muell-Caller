﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Timer = System.Timers.Timer;



var builder = WebApplication.CreateBuilder(args);
Dictionary<string, string> normalized = builder.Configuration.AsEnumerable()
    .Where(p => p.Key.Contains("__"))
    .ToDictionary(p => p.Key.Replace("__", ":"), p => p.Value);
builder.Configuration.AddInMemoryCollection(normalized);


builder.Logging.AddConsole();
builder.Services.AddControllers();

var app = builder.Build();
TelegramNotifier.serviceProvider = app.Services;

Timer timer = new Timer(1000 * 180);
timer.Elapsed += (sender, eventArgs) =>
{
    using (var scope = app.Services.CreateScope())
    {
        scope.ServiceProvider.GetService<AppointmentLoader>().LoadAppointments().Wait();
        scope.ServiceProvider.GetService<NotificationCreator>().CheckForNotification().Wait();
    }
};

using (var scope = app.Services.CreateScope())
{
    scope.ServiceProvider.GetService<AppointmentLoader>().LoadAppointments().Wait();
    scope.ServiceProvider.GetService<NotificationCreator>().CheckForNotification().Wait();
}
timer.AutoReset = true;
timer.Start();

    
    
    
// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseStaticFiles();

app.UseRouting();


app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});
app.Run();