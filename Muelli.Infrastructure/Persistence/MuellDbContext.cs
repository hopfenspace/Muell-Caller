using Microsoft.EntityFrameworkCore;
using Muelli.Application;
using Muelli.Domain;

namespace Muelli.Infrastructure.Persistence;

public class MuellDbContext : DbContext, IMuellDbContext
{
    public MuellDbContext(DbContextOptions<MuellDbContext> dbContextOptions) : base(dbContextOptions)
    {
    }

    public DbSet<Appointment> Appointments { get; set; }
    public DbSet<Notification> Notifications { get; set; }
    public async Task<int> SaveChangesAsync()
    {
        return await base.SaveChangesAsync();
    }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Appointment>().HasMany(x => x.Notifications);
    }
}