using Microsoft.EntityFrameworkCore;

namespace Muelli.Infrastructure.Persistence;

public class MuellSqliteDbContext:MuellDbContext
{
    public MuellSqliteDbContext(DbContextOptions<MuellDbContext> dbContextOptions) : base(dbContextOptions)
    {
    }
}