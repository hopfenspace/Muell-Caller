using Microsoft.EntityFrameworkCore;

namespace Muelli.Infrastructure.Persistence;

public class MuellSqlDbContext:MuellDbContext
{
    public MuellSqlDbContext(DbContextOptions<MuellDbContext> dbContextOptions) : base(dbContextOptions)
    {
    }
}