using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Muelli.Infrastructure.Persistence
{
    public class SqlDesignTimeDbContextFactory : IDesignTimeDbContextFactory<MuellSqlDbContext>
    {
        public MuellSqlDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MuellDbContext>();
            const string connectionString = "Sql";
            builder.UseSqlServer(connectionString);
            return new MuellSqlDbContext(builder.Options);
        }
    }
    public class SqliteDesignTimeDbContextFactory : IDesignTimeDbContextFactory<MuellSqliteDbContext>
    {
        public MuellSqliteDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MuellDbContext>();
            const string connectionString = "Data Source";
            builder.UseSqlite(connectionString);
            return new MuellSqliteDbContext(builder.Options);
        }
    }
}