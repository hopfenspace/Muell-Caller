using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Muelli.Application.Configs;
using Muelli.Infrastructure.Config;
using Muelli.Infrastructure.Persistence;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;

namespace Muelli.Infrastructure;

public class TelegramRunner
{
    private readonly BotConfig _botConfig;
    private readonly TelegramConfigs _telegramConfigs;
    private readonly IServiceProvider _serviceProvider;
    private readonly TelegramBotClient _botClient;

    public TelegramRunner(BotConfig botConfig, TelegramConfigs telegramConfigs, IServiceProvider serviceProvider)
    {
        _botConfig = botConfig;
        _telegramConfigs = telegramConfigs;
        _serviceProvider = serviceProvider;
        _botClient = new TelegramBotClient(_botConfig.Token);
    }

    public void Start()
    {
        var receiverOptions = new ReceiverOptions
        {
            AllowedUpdates = { }
        };
        _botClient.StartReceiving(
            HandleUpdateAsync,
            HandleErrorAsync,
            receiverOptions);
    }

    private async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update,
        CancellationToken cancellationToken)
    {
        try
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<MuellDbContext>();
                if (update.Message != null)
                {
                    Console.WriteLine(update.Message.Text);
                    if (update.Message.Text == "/start@wgmuellbot" || update.Message.Text == "/start")
                    {
                        await botClient.SendTextMessageAsync(update.Message.Chat.Id,
                            $"Schicke deine Gruppen Id {update.Message.Chat.Id} an Quirin",
                            cancellationToken: cancellationToken);
                    }
                    else if (update.Message.Text == "/showmuell@wgmuellbot" || update.Message.Text == "/showmuell")
                    {
                        var config =
                            _telegramConfigs.Configs.SingleOrDefault(
                                x => x.GroupId == update.Message.Chat.Id.ToString());
                        if (config == null)
                        {
                            await botClient.SendTextMessageAsync(update.Message.Chat.Id,
                                "Deine Gruppe ist nicht im Bot hinterlegt", cancellationToken: cancellationToken);
                        }

                        var appointments = await dbContext.Appointments
                            .Where(x => x.StartTime.Date > DateTime.Now.Date.AddDays(-1))
                            .Where(x => x.ConfigId == config!.ConfigId)
                            .OrderBy(x => x.StartTime).Take(10).ToListAsync(cancellationToken);
                        var result = "";
                        foreach (var appointment in appointments)
                        {
                            result += appointment.StartTime.Day.ToString().PadLeft(2, '0') + "." +
                                      appointment.StartTime.Month.ToString().PadLeft(2, '0') + " " + appointment.Text +
                                      Environment.NewLine;
                        }

                        await botClient.SendTextMessageAsync(update.Message.Chat.Id, result,
                            cancellationToken: cancellationToken);
                    }
                }
                else
                {
                    try
                    {
                        Guid appointmentId = Guid.Parse(update.CallbackQuery.Data);
                        var appointment =
                            await dbContext.Appointments.SingleOrDefaultAsync(x => x.AppointmentId == appointmentId,
                                cancellationToken);
                        if (appointment == null) return;
                        appointment.DoAppointment(update.CallbackQuery.From.Id.ToString());
                        await dbContext.SaveChangesAsync(cancellationToken);
                        try
                        {
                            await botClient.EditMessageTextAsync(update.CallbackQuery.Message.Chat.Id.ToString(),
                                update.CallbackQuery.Message.MessageId, $"Danke {update.CallbackQuery.From.Username}",
                                cancellationToken: cancellationToken);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }

    private Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception,
        CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}