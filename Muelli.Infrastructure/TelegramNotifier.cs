using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Muelli.Application.Configs;
using Muelli.Application.Services;
using Muelli.Domain;
using Muelli.Infrastructure.Config;
using Muelli.Infrastructure.Persistence;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types.ReplyMarkups;

namespace Muelli.Infrastructure;

public class TelegramNotifier : INotificationSink
{
    private readonly BotConfig _botConfig;
    private readonly TelegramConfigs _telegramConfigs;
    private readonly TelegramBotClient _botClient;

    public TelegramNotifier(BotConfig botConfig, TelegramConfigs telegramConfigs)
    {
        _botConfig = botConfig;
        _telegramConfigs = telegramConfigs;
        _botClient = new TelegramBotClient(_botConfig.Token);
    }


    public async Task DoNotification(Appointment appointment)
    {
        try
        {
            InlineKeyboardMarkup inlineKeyboard = new(new[]
            {
                new[]
                {
                    InlineKeyboardButton.WithCallbackData("Hab ich gemacht", appointment.AppointmentId.ToString()),
                }
            });
            var config = _telegramConfigs.Configs.Single(x => x.ConfigId == appointment.ConfigId);
            await _botClient.SendTextMessageAsync(config.GroupId,
                $"Bitte {appointment.Text.Trim()} rausstellen. Am {appointment.StartTime.Day.ToString().PadLeft(2, '0')}.{appointment.StartTime.Month.ToString().PadLeft(2, '0')} ist Abholung.",
                replyMarkup: inlineKeyboard);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}