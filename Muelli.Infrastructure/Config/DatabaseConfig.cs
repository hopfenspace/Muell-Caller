namespace Muelli.Infrastructure.Config;

public class DatabaseConfig
{
    public string ConnectionString { get; set; }
    public string Type { get; set; }
}