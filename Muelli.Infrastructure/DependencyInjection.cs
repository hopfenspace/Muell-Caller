using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Muelli.Application;
using Muelli.Application.Configs;
using Muelli.Application.Services;
using Muelli.Infrastructure.Config;
using Muelli.Infrastructure.Persistence;

namespace Muelli.Infrastructure;

public static class DependencyInjection
{
    public static void ConfigureServices(this IServiceCollection serviceCollection, IConfiguration configuration)
    {
        serviceCollection.AddSingleton(configuration.GetRequiredSection("BotConfig").Get<BotConfig>());
        serviceCollection.AddSingleton(configuration.GetRequiredSection("NotificationConfig")
            .Get<NotificationConfig>());
        serviceCollection.AddSingleton(configuration.GetRequiredSection("DatabaseConfig").Get<DatabaseConfig>());
        serviceCollection.AddSingleton(configuration.GetRequiredSection("TelegramConfigs").Get<TelegramConfigs>());
        var dbConfig = configuration.GetRequiredSection("DatabaseConfig")
            .Get<DatabaseConfig>();
        var optionsBuilder = new DbContextOptionsBuilder<MuellDbContext>();
        if (dbConfig.Type == "sqlite")
        {
            optionsBuilder.UseSqlite(dbConfig.ConnectionString);
            serviceCollection.AddDbContext<MuellSqliteDbContext>();
            serviceCollection.AddScoped<MuellDbContext,MuellSqliteDbContext>();
        }
        else if (dbConfig.Type == "sql")
        {
            optionsBuilder.UseSqlServer(dbConfig.ConnectionString);
            serviceCollection.AddDbContext<MuellSqlDbContext>(opt =>
                opt.UseSqlServer(dbConfig.ConnectionString));
            serviceCollection.AddScoped<MuellDbContext,MuellSqlDbContext>();
        }
        else
        {
            throw new Exception("DatabaseTyp not found");
        }

        serviceCollection.AddScoped<DbContextOptions<MuellDbContext>>(s => optionsBuilder.Options);

        serviceCollection.AddScoped<IMuellDbContext, MuellDbContext>();
        serviceCollection.AddScoped<AppointmentLoader>();
        serviceCollection.AddScoped<TelegramRunner>();
        serviceCollection.AddScoped<NotificationCreator>();
        serviceCollection.AddScoped<INotificationSink, TelegramNotifier>();
    }
}